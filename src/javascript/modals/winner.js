import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showWinnerModal(fighter) {
  const title = 'Winner';
  const { name, source } = fighter;

  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const attributes = { src: source };
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });

  nameElement.innerText = name;
  bodyElement.append(nameElement, imageElement);
  showModal({ title, bodyElement });
}