export function fight(firstFighter, secondFighter) {
  let firstFighterHealth = firstFighter.health,
   secondFighterHealth = secondFighter.health;

  while(firstFighterHealth > 0 && secondFighterHealth > 0) {
    secondFighterHealth -= getDamage(firstFighter, secondFighter);
    if(secondFighterHealth <= 0) {
      return firstFighter;
    }
      firstFighterHealth -= getDamage(secondFighter, firstFighter);
  }
   return secondFighter;
}

export function getDamage(attacker, enemy) {
  const damage = getHitPower(attacker) - getBlockPower(enemy);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = getRandomNumber(1,2);
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = getRandomNumber(1,2);
  return fighter.defense * dodgeChance;
}

function getRandomNumber(from, to) {
  return Math.random() * (to - from) + from;
}